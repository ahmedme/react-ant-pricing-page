import { Link } from 'react-router-dom'
import { ReactComponent as Logo } from './logo.svg'
import { Layout, Row, Col, List, Typography } from 'antd'
const { Footer } = Layout
const { Title } = Typography

function PageFooter({ footerData = [] }) {
  const today = new Date()
  const thisYear = today.getFullYear()

  const footerColLength = Math.floor(
    24 / (footerData.length + 1) < 4 ? 4 : 24 / (footerData.length + 1)
  )
  return (
    <Footer>
      <Row>
        <Col span={footerColLength}>
          <div className='text-center'>
            <Logo />
            <div>&copy; 2017–{thisYear}</div>
          </div>
        </Col>
        {footerData.map((e, k) => (
          <Col span={footerColLength} key={k}>
            <List
              size='large'
              header={<Title level={5}>{e.title}</Title>}
              dataSource={e.items}
              renderItem={item => (
                <List.Item>
                  <Link to={item.link}>{item.text}</Link>
                </List.Item>
              )}
            />
          </Col>
        ))}
      </Row>
    </Footer>
  )
}
export default PageFooter
