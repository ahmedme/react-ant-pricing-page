import { Layout, Menu, Row } from 'antd'
import { Link } from 'react-router-dom'
import { ReactComponent as Logo } from './logo.svg'

const { Header } = Layout

function NavBar({ navMenue = [] }) {
  return (
    <Header>
      <div className='container'>
        <Row justify='space-between'>
          <Link to='/'>
            <div className='logo'>
              <Logo />
              <span>Pricing example</span>
            </div>
          </Link>
          <Menu mode='horizontal' defaultSelectedKeys={['2']}>
            {navMenue.map((e, k) => (
              <Menu.Item key={k} icon={e.icon}>
                <Link to={e.link}>{e.text}</Link>
              </Menu.Item>
            ))}
          </Menu>
        </Row>
      </div>
    </Header>
  )
}

export default NavBar
