import { offset, navMenue, footerData } from './data'
import './App.css'
import NavBar from './NavBar'
import Welcome from './Welcome'
import PricingCards from './PricingCards'
import Compare from './Compare'

import PageFooter from './PageFooter'
import { Layout } from 'antd'

const { Content } = Layout

const App = () => {
  return (
    <Layout>
      <NavBar offset={offset} navMenue={navMenue} />

      <Content>
        <Welcome
          title='Pricing'
          paragraph='Quickly build an effective pricing table for your potential customers with this Bootstrap example. It’s built with default Bootstrap components and utilities with little customization.'
        />
        <PricingCards />
        <Compare />
      </Content>
      <PageFooter footerData={footerData} />
    </Layout>
  )
}

export default App
