import { Typography } from 'antd'
const { Title, Paragraph } = Typography

function Welcome({ title = '', paragraph = '' }) {
  return (
    <div className='welcome-panel'>
      <Title level={1}>{title}</Title>
      <Paragraph>{paragraph}</Paragraph>
    </div>
  )
}

export default Welcome
