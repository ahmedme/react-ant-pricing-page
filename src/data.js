import React from 'react'
import {
  AppstoreOutlined,
  BarChartOutlined,
  CloudOutlined,
  ShopOutlined,
  TeamOutlined,
  UserOutlined,
  UploadOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons'

const Icons = [
  <AppstoreOutlined />,
  <BarChartOutlined />,
  <CloudOutlined />,
  <ShopOutlined />,
  <TeamOutlined />,
  <UserOutlined />,
  <UploadOutlined />,
  <VideoCameraOutlined />,
]

export const offset = 2
export const navMenue = ['Features', 'Enterprise', 'Support', 'Pricing'].map((e, k) => ({
  text: e,
  link: '/' + e.toLowerCase(),
  icon: Icons[k],
}))

export const footerData = [
  {
    title: 'Features',
    items: [
      { link: '/', text: 'Cool stuff' },
      { link: '/', text: 'Random feature' },
      { link: '/', text: 'Team feature' },
      { link: '/', text: 'Stuff for developers' },
      { link: '/', text: 'Another one' },
      { link: '/', text: 'Last time' },
    ],
  },
  {
    title: 'Resources',
    items: [
      { link: '/', text: 'Resource' },
      { link: '/', text: 'Resource name' },
      { link: '/', text: 'Another resource' },
      { link: '/', text: 'Final resource' },
    ],
  },
  {
    title: 'About',
    items: [
      { link: '/', text: 'Team' },
      { link: '/', text: 'Locations' },
      { link: '/', text: 'Privacy' },
      { link: '/', text: 'Terms' },
    ],
  },
  // {
  //   title: 'About',
  //   items: [
  //     { link: '/', text: 'Team' },
  //     { link: '/', text: 'Locations' },
  //     { link: '/', text: 'Privacy' },
  //     { link: '/', text: 'Terms' },
  //   ],
  // },
]
