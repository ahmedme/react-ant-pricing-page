import { CheckOutlined } from '@ant-design/icons'
import { Table, Typography } from 'antd'
const { Title } = Typography

export default function Compare(props) {
  const dataSource = [
    { feature: 'Public', free: true, pro: true, entreprise: true },
    { feature: 'Private', free: false, pro: true, entreprise: true },
    { feature: 'Permissions', free: true, pro: true, entreprise: true },
    { feature: 'Sharing', free: false, pro: true, entreprise: true },
    { feature: 'Unlimited members ', free: false, pro: true, entreprise: true },
    { feature: 'Extra security', free: false, pro: false, entreprise: true },
  ]

  const columns = [
    { title: '', dataIndex: 'feature', key: 'feature' },
    {
      title: 'Free',
      dataIndex: 'free',
      key: 'free',
      render: value => (value ? <CheckOutlined /> : ''),
    },
    {
      title: 'Pro',
      dataIndex: 'pro',
      key: 'pro',
      render: value => (value ? <CheckOutlined /> : ''),
    },
    {
      title: 'Enterprise',
      dataIndex: 'entreprise',
      key: 'entreprise',
      render: value => (value ? <CheckOutlined /> : ''),
    },
  ]

  return (
    <div className='comparatif'>
      <Title
        level='2'
        style={{
          textAlign: 'center',
          marginBottom: '1em',
        }}>
        Compare plans
      </Title>
      <Table
        pagination={false}
        dataSource={dataSource.map((e, k) => ({ ...e, key: k }))}
        columns={columns}
      />
    </div>
  )
}
