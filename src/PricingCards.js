import { Row, Col, Card, Button, Typography } from 'antd'
const { Title, Text } = Typography

function PricingCards(props) {
  const cardData = [
    {
      title: 'Free',
      price: '0',
      unit: 'mo',
      items: ['10 users included', '2 GB of storage', 'Email support', 'Help center access'],
      button: 'Sign up for free',
      buttonGhost: true,
      cardVarient: 'border-grey',
      border: false,
      headStyle: {},
    },
    {
      title: 'Pro',
      price: '15',
      unit: 'mo',
      items: [
        '20 users included',
        '10 GB of storage',
        'Priority email support',
        'Help center access',
      ],
      button: 'Get started',
      buttonGhost: false,
      cardVarient: 'border-grey',
      border: true,
      headStyle: {},
    },
    {
      title: 'Enterprise',
      price: '29',
      unit: 'mo',
      items: [
        '30 users included',
        '15 GB of storage',
        'Phone and email support',
        'Help center access',
      ],
      button: 'Contact us',
      buttonGhost: false,
      cardVarient: 'border-primary',
      border: true,
      headStyle: {
        background: 'var(--primary-color)',
        color: 'white',
      },
    },
  ]
  return (
    <Row gutter={[24, 24]}>
      {cardData.map((e, k) => (
        <Col key={k} xs={24} md={24 / cardData.length}>
          <Card
            bodyStyle={{ textAlign: 'center' }}
            title={e.title}
            className={e.cardVarient}
            headStyle={{ ...e.headStyle, textAlign: 'center', fontSize: '1.5em' }}>
            <Title>
              $<Text strong={true}>{e.price}</Text>/{e.unit}
            </Title>

            <ul>
              {e.items.map((i, ik) => (
                <li key={ik}>{i}</li>
              ))}
            </ul>
            <Button ghost={e.buttonGhost} type='primary' size='large' block>
              {e.button}
            </Button>
          </Card>
        </Col>
      ))}
    </Row>
  )
}

export default PricingCards
